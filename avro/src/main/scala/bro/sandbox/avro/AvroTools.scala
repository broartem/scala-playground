package bro.sandbox.avro

import java.io._

import org.apache.avro.Schema
import org.apache.avro.generic._
import org.apache.avro.io._

import scala.util.Try

object AvroTools
{
  @throws(classOf[java.io.IOException])
  def jsonToAvro(json: String, schemaStr: String) {
    val schema = new Schema.Parser().parse(schemaStr)
    val decoder = DecoderFactory.get.jsonDecoder(schema, json)
  }
}

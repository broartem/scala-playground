package bro.sandbox.avro

import java.io.ByteArrayOutputStream

import org.apache.avro.io.{ExtendedJsonEncoder, ExtendedJsonDecoder}
import org.apache.avro.specific.{SpecificDatumWriter, SpecificRecord, SpecificDatumReader}
import org.scalatest.{FlatSpec, Matchers}
import ru.svyaznoy.libs.serialization.AvroDecoder
import bro.sandbox.avro.data.SampleOrder

class AvroSchemaTest extends FlatSpec with Matchers {
  def jsonToAvro[T](clazz: Class[T], data: String): T = {
    val reader = new SpecificDatumReader[T](clazz)
    val schema = reader.getSpecificData.getSchema(clazz)
    val decoder = new ExtendedJsonDecoder(schema, data)
    reader.read(null.asInstanceOf[T], decoder)
  }

  "Zolyfarkas Avro" must "consume jsons with [null, type] fields withoud type inside a key" in {
    val dataJson = """{
    |"id": "",
    |"price": 7200.0
    |}""".stripMargin

    val order = jsonToAvro(classOf[SampleOrder], dataJson)
    order.getPrice should equal (7200.0)
  }

  "Zolyfarkas Avro" must "set defaults for missed fields of simple type from an input json" in {
    val dataJsonSkippedId = """{
    |"price": 7200.0
    |}""".stripMargin

    val order = jsonToAvro(classOf[SampleOrder], dataJsonSkippedId)
    order.getId.toString should equal ("")
  }

  "Zolyfarkas Avro" must "fill set to null fields of type [null, sometype] with default null" in {
    val dataJsonSkippedPrice = """{
    |"id": "o1"
    |}""".stripMargin

    val order = jsonToAvro(classOf[SampleOrder], dataJsonSkippedPrice)
    order.getPrice should equal (null)
  }

  "Zolyfarkas Avro" must "fill all the default values if they are not presented in an input json" in {
    val dataJsonSkippedIdAndPrice = """{
    |}""".stripMargin

    val order = jsonToAvro(classOf[SampleOrder], dataJsonSkippedIdAndPrice)
    order.getId.toString should equal ("")
    order.getPrice should equal (null)
  }

  "Zolyfarkas Avro" must "produce jsons with [null, type] fields withoud type inside a key" in {
    val order = new SampleOrder
    order.setId("o1")
    order.setPrice(7200.0)

    val dataJsonExpected = """{
    |"id":"o1",
    |"price":7200.0
    |}""".stripMargin.replaceAll("[\r\n]+", "")

    val schema = SampleOrder.getClassSchema
    val writer = new SpecificDatumWriter[SampleOrder](schema)
    val output = new ByteArrayOutputStream()
    val encoder = new ExtendedJsonEncoder(schema, output)
    writer.write(order, encoder)
    encoder.flush()
    new String(output.toByteArray) should equal (dataJsonExpected)
  }

  "AvroDecoder from sv-libs" must "encode/decode Zolyfarkas Avro" in {
    val avroDecoder = new AvroDecoder[SampleOrder](classOf[SampleOrder])
    val order = SampleOrder.newBuilder
      .setId("o1")
      .setPrice(500.0)
      .build
    val orderBytes = avroDecoder.encode(order)
    val orderRestored = avroDecoder.decode(orderBytes)
    orderRestored should equal (order)
  }

  "AvroDecoder from sv-libs" must "encode/decode Zolyfarkas Avro with unspecified fields" in {
    val avroDecoder = new AvroDecoder[SampleOrder](classOf[SampleOrder])
    val order = SampleOrder.newBuilder.build
    val orderBytes = avroDecoder.encode(order)
    val orderRestored = avroDecoder.decode(orderBytes)
    orderRestored should equal (order)
  }
}

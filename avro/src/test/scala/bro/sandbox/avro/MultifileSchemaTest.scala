package bro.sandbox.avro

import java.util

import org.apache.avro.io.ExtendedJsonDecoder
import org.apache.avro.specific.SpecificDatumReader
import org.scalatest.{FlatSpec, Matchers}
import bro.sandbox.avro.data._
import scala.collection.JavaConversions._

class MultifileSchemaTest extends FlatSpec with Matchers {
  "Zolyfarkas Avro" must "build order without exceptions" in {
    val order = MultifileOrder.newBuilder
      .setBasket(new util.ArrayList[MultifileOrderItem]())
      .build
  }

  "Zolyfarkas Avro" must "decode ExtendedOrder with all fields filled in" in {
    val dataJson = """{
    |"id": "o1",
    |"status": "ACCEPTED",
    |"basket": [
    |  {
    |    "product_id": "p1",
    |    "quantity": 10,
    |    "price": 210.0,
    |    "product_description": {"category": "Phones"}
    |  },
    |  {
    |    "product_id": "p2",
    |    "quantity": 2,
    |    "price": 41.1,
    |    "product_description": {"category": "Food"}
    |  }
    |]
    |}""".stripMargin

    val schema = MultifileOrder.getClassSchema
    val reader = new SpecificDatumReader[MultifileOrder](schema)
    val decoder = new ExtendedJsonDecoder(schema, dataJson)
    val order = reader.read(null, decoder)

    order.getId.toString should equal ("o1")
    order.getStatus should equal (MultifileStatus.ACCEPTED)
    order.getBasket.size should equal (2)

    order.getBasket()(0).getProductId.toString should equal ("p1")
    order.getBasket()(0).getQuantity should equal (10)
    order.getBasket()(0).getPrice should equal (210.0)
    order.getBasket()(0).getProductDescription.getCategory.toString should equal ("Phones")

    order.getBasket()(1).getProductId.toString should equal ("p2")
    order.getBasket()(1).getQuantity should equal (2)
    order.getBasket()(1).getProductDescription.getCategory.toString should equal ("Food")
  }
}


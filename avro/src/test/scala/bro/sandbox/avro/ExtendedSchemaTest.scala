package bro.sandbox.avro

import java.util

import org.apache.avro.io.{ExtendedJsonEncoder, ExtendedJsonDecoder}
import org.apache.avro.specific.{SpecificDatumWriter, SpecificRecord, SpecificDatumReader}
import org.scalatest.{FlatSpec, Matchers}
import bro.sandbox.avro.data._
import scala.collection.JavaConversions._

class ExtendedSchemaTest extends FlatSpec with Matchers {
  "Zolyfarkas Avro" must "build order without exceptions" in {
    val order = ExtendedOrder.newBuilder
      .setBasket(new util.ArrayList[Item]())
      .build
  }

  "Zolyfarkas Avro" must "decode ExtendedOrder with all fields filled in" in {
    val dataJson = """{
    |"id": "o1",
    |"status": "ACCEPTED",
    |"basket": [
    |  {"product_id": "p1", "quantity": 10, "price": 210.0},
    |  {"product_id": "p2", "quantity": 2, "price": 41.1}
    |],
    |"user_info": {
    |  "first_name": {"date_changed": "2015-08-03 21:00:00", "value": "Nancy"},
    |  "last_name": {"date_changed": "2015-08-03 21:00:00", "value": "Black"}
    |}
    |}""".stripMargin

    val schema = ExtendedOrder.getClassSchema
    val reader = new SpecificDatumReader[ExtendedOrder](schema)
    val decoder = new ExtendedJsonDecoder(schema, dataJson)
    val order = reader.read(null, decoder)

    order.getId.toString should equal ("o1")
    order.getStatus should equal (Status.ACCEPTED)
    order.getBasket.size should equal (2)

    order.getBasket()(0).getProductId.toString should equal ("p1")
    order.getBasket()(0).getQuantity should equal (10)
    order.getBasket()(0).getPrice should equal (210.0)

    order.getBasket()(1).getProductId.toString should equal ("p2")
    order.getBasket()(1).getQuantity should equal (2)
    order.getBasket()(1).getPrice should equal (41.1)
  }
}


package org.bitbucket.pg.scala.scalaz

import scalaz._

object RecordParserWithStateMonad extends App {
  case class Record(id: String, bool: Boolean, date: java.util.Date, num1: Int, num2: Long)
  case class RecordValidation(valid: Boolean, msgs: List[String])
  case class ParserState(record: Record, validation: RecordValidation)

  def parseId(id: String) = State[ParserState, Unit] = {
    case ParserState(r, v) if id == "" => ParserState(r, RecordValidation(valid = false, "Invalid id" :: v.msgs))
    case ParserState(r, v) => ParserState(r.copy(id = id), v)
  }

//  def parse(input: String): (Record, RecordValidation) = {
////    val
//    input.split(",") match {
//      case (id: String, bool: String, date: String, num1: String, num2: String) =>
//
//    }
//  }

  override def main(args: Array[String]) {
//    println(parse("11,true,2016-01-01 12:00:01"))
//    println(parse("11,true,2016-01-01 wrong"))
  }
}
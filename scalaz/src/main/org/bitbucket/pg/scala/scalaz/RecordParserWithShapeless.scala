package org.bitbucket.pg.scala.scalaz

import scala.util.{Failure, Success, Try}

object RecordParserWithShapeless extends App {

  case class Record(id: String, bool: Boolean, date: java.util.Date)
  case class RecordValidation(valid: Boolean, msgs: List[String])

  object ParsedUnit {
    case class ParsedUnit[T](item: Option[T], valid: Boolean, msg: Option[String])

    def ParserSuccess[T](v: T) = ParsedUnit(Option(v), valid = true, Option.empty)
    def ParserFailure[T](msg: String) = ParsedUnit(Option.empty[T], valid = false, Option(msg))

    def TryParse[T](r: => T, msg: String): ParsedUnit[T] = Try(r) match {
      case Success(b) => ParserSuccess(b)
      case Failure(_) => ParserFailure(msg)
    }
  }
  import ParsedUnit._

  def parseId(id: String): ParsedUnit[String] = id match {
    case v if v != "" => ParserSuccess(v)
    case _ => ParserFailure("Empty id")
  }

  def parseBool(bool: String) = TryParse(bool.toBoolean, "Wrong bool")

  def parseDate(date: String) = {
    val format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    TryParse(format.parse(date), "Wrong Date")
  }

  def parse(input: String): (Option[Record], RecordValidation) = {
    import shapeless._
    import ops.tuple.FlatMapper

    object getValid extends Poly1 {
      implicit def caseAny[T] = at[ParsedUnit[T]](_.valid)
    }

    object getMessages extends Poly1 {
      implicit def caseAny[T] = at[ParsedUnit[T]](_.msg)
    }

    object getValues extends Poly1 {
      implicit def caseAny[T] = at[ParsedUnit[T]](_.item.get)
    }

    input.split(",") match {
      case Array(id: String, bool: String, date: String) =>
        val parsed = parseId(id) :: parseBool(bool) :: parseDate(date) :: HNil
        val valid = parsed.map(getValid).toList.forall(_ == true)
        val messages = parsed.map(getMessages).toList.flatten

        if (valid) (Option(Record.tupled(parsed.map(getValues).tupled)), RecordValidation(valid = true, List()))
        else (Option.empty, RecordValidation(valid = false, messages))
    }
  }

  override def main(args: Array[String]) {
    println(parse("11,true,2016-01-01 12:00:01"))
    println(parse("11,true,2016-01-01 wrong"))
  }
}

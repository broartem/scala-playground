package bro.sandbox.json

import java.util.Date
import org.json4s.JsonAST.JString
import org.json4s._
import org.json4s.jackson.{Json, Serialization}
import org.json4s.jackson.Serialization._
import org.json4s.jackson.JsonMethods._

object Json4s extends App {
  trait AbstractNumber
  case class One(x: Int, date: Date) extends AbstractNumber
  case class Two(x: Int, date: Date) extends AbstractNumber
  case class Numbers(xs: Iterable[AbstractNumber])

  implicit val oneWrites = JsonWriter
  implicit val formats = Serialization.formats(NoTypeHints) +
    FieldSerializer[One]() + new NumbersSerializer

  class NumbersSerializer extends CustomSerializer[Numbers](format => (
    {
      case JObject(JField("@context", JString(context)) :: JField("xs", JArray(xs)) :: Nil) =>
//        new Numbers(xs.map(_.asInstanceOf[One]))
      throw new Error("Write only object")
    },
    {
      case x: Numbers =>
        JObject(JField("@context", JString("lala")) ::
          JField("xs", JArray(Extraction.decompose(x.xs) :: Nil)) :: Nil)
    }
  ))

//  implicit val formats = Serialization.formats(ShortTypeHints(List(classOf[One], classOf[Two])))

  val numbers = Numbers(List(
    One(1, new Date()),
    Two(2, new Date())
  ))

  println(writePretty(numbers))

//  val one = One(1, new Date)
//  println(pretty(render(one.asJValue)))
}

package bro.sandbox.json

import java.text.SimpleDateFormat
import java.util.{Date, TimeZone}
import spray.json._

object SprayJson extends App {
  abstract class AbstractNumber
  case class One(x: Int, date: Date) extends AbstractNumber
  case class Two(x: Int, date: Date) extends AbstractNumber
  case class Numbers(xs: Iterable[AbstractNumber])

  object MyJsonProtocol extends DefaultJsonProtocol {
    val formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    formatter.setTimeZone(TimeZone.getTimeZone("Europe/Moscow"))

    implicit object DateJsonFormat extends RootJsonFormat[Date] {
      def write(date: Date) = JsString(formatter.format(date))
      def read(value: JsValue) = throw new Error("Write-only object")
    }
    implicit val oneFormat = jsonFormat2(One)
    implicit val twoFormat = jsonFormat2(Two)

    implicit object AbstractNumberJsonFormat extends RootJsonFormat[AbstractNumber]{
      def write(n:AbstractNumber) = n match {
        case one:One => one.toJson
        case two:Two => two.toJson
      }
      def read(value:JsValue) = throw new Error("Write-only object")
    }
    implicit object NumbersJsonFormat extends RootJsonFormat[Numbers] {
      def write(n: Numbers) = JsObject(
        "@context" -> JsString("lalala"),
        "numbers" -> n.xs.toJson
      )
      def read(value: JsValue) = throw new Error("Write-only object")
    }
  }
  import MyJsonProtocol._

  val numbers = Numbers(List(One(1, new Date()),Two(2, new Date())))
  println(numbers.toJson.prettyPrint)
}

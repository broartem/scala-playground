package bro.sandbox.lang

object ImplicitOperator extends App
{
  // Error: value class may not be a member of another class
  // but value class is fast! Is it possible to overcome this limitation
  // to preserve performance?

//  trait RangeOperatorHolder {
//    implicit class RangeMaker(left: Int) extends AnyVal {
//      def -->(right: Int): Range = left to right
//    }
//  }

//  class My extends RangeOperatorHolder {
//    print(1 --> 10)
//  }
}
